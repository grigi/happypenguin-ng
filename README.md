michaelb's ideas on the next linux gaming tome
============







Getting started
-----



First, get your dev env set up:

1. This is a django / mezzanine project. if you aren't familiar with these then follow these steps closely.

2. make sure you have python (i hope so!), and `pip` installed

3. `pip install mezzanine` to get the necessary packages. Either sudo this to
install package globally, or if you know `virtualenv` you can do it that way.
The specific version is in requirements.txt.

4. To test try running `python manage.py` and you should geta  list of management commands

5. (optional) get fabric installed for remote administration with the default fabfile

Second, get the initial data:

1. Download bobz's archive ~400 MB

2. Untar it to some handy location (I untarred it to ./data, so that there
    exists a directory ./data/screenshots)

3. Download grigi's de-normalized JSON version of the DB. Unzip it to the same
location. [GET IT HERE](http://happypenguin.onkoistudios.com/discussion/5/de-normalized-db#Item_2)

Third, generate a dev sqlite DB and get 

1. make sure you have sqlite & python-sqlite installed

2. `python  mange.py createdb` -- you will be asked to create a testing admin
account. fill out that info as you see fit. you will also be asked about
installing some test pages. might as well do that! :)

3. `python manage.py import_hp_dump data/` where `data/` is where you put the
unzipped data from the previous section.  this will import everything into your
new test db.

4. `python manage.py runserver` to run a temporary server! Go to
`localhost:8000` and check it out :)



Style info
------

- [Pallet (chosen because of "retro" colors)](http://www.colourlovers.com/palette/53698/Its_a_Virtue)

- [Bootstrap theme (from above color scheme)](http://www.stylebootstrap.info/index.php?style=VMxlFu6B86U54mbXKRjho)

- Background from SubtleBackgrounds


--------------


                   .888888:.
                   88888.888.       ..ooo00000oo..
                  .8888888888    000000000000000000oo..       ..oo00
                  8' `88' `888   0000''' |  /'''00000000000000000000
            HH    8 8 88 8 888   000     | /    00    \  ''00''  000
     HI     |H    8:.,::,.:888   000     |/     00     \ ________000
     HH,    |H   .8`::::::'888   000___    /|   00    /   \      000
     |HH,   |H   88  `::'  888   000  /   / |   00----____/      000
     | HH,  HH  .88        `888. 000 /   /  |   00   /    \      000
     |  YHHHHH.88'   .::.  .:8888.00..ooo0000oo.00  /      \     000
      \    YHH888.'   :'    `'88:88.0000000000000000oo..    \ ..o000
       \==/ .8888'    '        88:88.'  \  |  ''00000000000000000000
        == .8888H,    .        88:888    \ |    00    \   ''   / 000
           `8888HH,   :        8:888'     \|    00___  \  /   /  000
            `.:.8HH,  .       .::888'------o----00  /   \/   /___000
           .:::::88H  `      .:::::::.     /\   00 /             000
          .::::::.8         .:::::::::    /  \  00/    /|   |\   000
          :::::::::..     .:::::::::'..oo00oo.. 00  /|/ |   | \  000
           `:::::::::88888:::::::'00000000000000000oo.. |   | ..oo00
              rs`:::'       `:'  000'''      ' Rafal Slubowski000
                                                   ''00''


