from tome.models import Game
from django.contrib import admin
from django.core.management.base import BaseCommand , CommandError
import sys
import shutil
import os.path
from django.contrib.auth.models import User
from datetime import datetime
from django.conf import settings

from tome.util import warning, trace, error

import json


def new_user(name):
    if not User.objects.filter(username=name):
        joetest = User.objects.create_user(name,
                "%s@test.com"%name, "asdf")
        joetest.is_active = True
        joetest.save()
    else:
        joetest = User.objects.get(username=name)
    return joetest

USE = """
How to use: Download bobz's DB dump and untar it somewhere easy to access. (ie
'./data/). Download specify path to download of both gigri's denormalized JSON
files and untar to the same location as bobz's DB dump. Specify the path to
this folder as the only argument to this command.
"""


def copy_image(source, just_link=True):
    name = os.path.split(source)[-1]
    dest = os.path.join(settings.MEDIA_ROOT, "screenshots", name)
    dest_dir = os.path.join(settings.MEDIA_ROOT, "screenshots")
    try:
        os.makedirs(dest_dir)
    except OSError:
        pass

    trace("%s %s -> %s" % (("Copy", "Linking")[int(just_link)], source, dest))
    if just_link:
        if not os.path.lexists(dest):
            os.link(source, dest)
    else:
        shutil.copyfile(source, dest)

    return os.path.join("screenshots", name)

text = ''
class Command(BaseCommand):
    args = "<path-to-unzipped-json-dump>"
    def handle(self, path=None, force_copy=False, **options):
        if not path:
            print USE
            error("Did not specify path")

        # optional arg to force it to copy as opposed to link
        just_link = force_copy != 'force_copy'


        # First checking if a sqlite DB exists there
        json_path = os.path.join(path, 'games.json')
        trace("First checking for %s..." % path)
        if not os.path.exists(json_path):
            print USE
            error("%s path does not exist. Make sure " % json_path+
                    "you download gigri's denormalized "+
                    "json.")

        screenshots_path = os.path.join(path, 'screenshots')
        if not os.path.exists(screenshots_path):
            print USE
            error("%s path does not exist. "  % screenshots_path +
                    " Make sure that the screenshot "+
                    "directory from bobz's original dump is in the same "+
                    "place as the json.")

        games = json.load(open(json_path))


        trace("Making a dummy user for bobz")
        bobz = new_user("bobz")


        for game in games:
            # For now until we better analyze the urls data we should just grab
            # the top one
            url = game.get('urls', '') and game['urls'][0].get('url')
            is_free = game.get('license') == 'free'
            if game.get('screenshot'):
                source = os.path.join(screenshots_path, game['screenshot'])
                # copy over to new location now
                if not os.path.exists(source):
                    warning("Could not find screenshot '%s' " % source)
                    image = ''
                else:
                    image = copy_image(source, just_link)

            else:
                image = ''
            cost = game.get('cost', '').strip("$USD") or "0.00"
            try:
                # quick lil validation
                cost = "%i.%i" % tuple(map(int, cost.split('.')))
            except Exception as e:
                warning("Unusual cost string: %s" % cost)
                trace("received exception: %s" % repr(e))
                cost = "0.00"
            Game.objects.create(
                    title=game.get('title'),
                    description=game.get('short_description'),
                    content=game.get('description'),
                    website_link=url,
                    content_is_free=is_free,
                    engine_is_free=is_free,
                    latest_version=game.get('version', ''),
                    publish_date=game.get('date_submitted'),
                    user=bobz,

                    # Screenshot:
                    featured_image=image,
                    cost=cost,
                )

