from tome.models import *
from django.contrib import admin


class GameAdmin(admin.ModelAdmin):
	ordering = ['publish_date']

admin.site.register(Game, GameAdmin)

