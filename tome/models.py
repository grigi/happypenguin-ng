from django.db import models
from mezzanine.blog.models import BlogPost
from django.utils.translation import ugettext_lazy as _

from mezzanine.conf import settings
from mezzanine.core.fields import FileField
from mezzanine.core.models import Displayable, Ownable, RichText, Slugged
from mezzanine.generic.fields import CommentsField, RatingField
from mezzanine.utils.models import AdminThumbMixin, upload_to

"""
licenses = (
    ('OSI Approved',
        'OSI Approved',
        'Academic Free License (AFL)',
        'Apache Software License',
        'Apple Public Source License',
        'Artistic License',
        'Attribution Assurance License',
        'BSD License',
        'Common Public License',
        'Eiffel Forum License',
        'European Union Public Licence 1.0 (EUPL 1.0)',
        'European Union Public Licence 1.1 (EUPL 1.1)',
        'GNU Affero General Public License v3',
        'GNU Affero General Public License v3 or later (AGPLv3+)',
        'GNU Free Documentation License (FDL)',
        'GNU General Public License (GPL)',
        'GNU General Public License v2 (GPLv2)',
        'GNU General Public License v2 or later (GPLv2+)',
        'GNU General Public License v3 (GPLv3)',
        'GNU General Public License v3 or later (GPLv3+)',
        'GNU Lesser General Public License v2 (LGPLv2)',
        'GNU Lesser General Public License v2 or later (LGPLv2+)',
        'GNU Lesser General Public License v3 (LGPLv3)',
        'GNU Lesser General Public License v3 or later (LGPLv3+)',
        'GNU Library or Lesser General Public License (LGPL)',
        'IBM Public License',
        'Intel Open Source License',
        'ISC License (ISCL)',
        'Jabber Open Source License',
        'MIT License',
        'MITRE Collaborative Virtual Workspace License (CVW)',
        'Motosoto License',
        'Mozilla Public License 1.0 (MPL)',
        'Mozilla Public License 1.1 (MPL 1.1)',
        'Mozilla Public License 2.0 (MPL 2.0)',
        'Nethack General Public License',
        'Nokia Open Source License',
        'Open Group Test Suite License',
        'Python License (CNRI Python License)',
        'Python Software Foundation License',
        'Qt Public License (QPL)',
        'Ricoh Source Code Public License',
        'Sleepycat License',
        'Sun Industry Standards Source License (SISSL)',
        'Sun Public License',
        'University of Illinois/NCSA Open Source License',
        'Vovida Software License 1.0',
        'W3C License',
        'X.Net License',
        'zlib/libpng License',
        'Zope Public License',
    ),
    ('Non-legal', (
        'Free For Educational Use',
        'Free For Home Use',
        'Free for non-commercial use',
        'Freely Distributable',
        'Free To Use But Restricted',
        'Freeware',
    ),
    ('Creative Commons', (
        'Creative Commons Attribution',
        'Creative Commons Attribution-ShareAlike',
        'Creative Commons Attribution-NoDerivs',
        'Creative Commons Attribution-NonCommercial',
        'Creative Commons Attribution-NonCommercial-ShareAlike',
        'Creative Commons Attribution-NonCommercial-NoDerivs',
        'CC0 1.0 Universal (CC0 1.0) Public Domain Dedication',
    ),
    ('Miscellaneous',
        'Repoze Public License',
        'Aladdin Free Public License (AFPL)',
        'DFSG approved',
        'Eiffel Forum License (EFL)',
        'Netscape Public License (NPL)'
        'Nokia Open Source License (NOKOS)'
    )
)


_license_choices = [
    '', 'Proprietary License',
    'pd', 'Public Domain',
]
"""

def get_licenses():
    # NOTE: for now we just simplify
    return (('floss', 'Free Software'), ('prop', 'Proprietary'))



class LicenseField(models.CharField):
    def __init__(*a, **k):
        choices = LICENSE_CHOICES
        if 'dont_allow' in k:
            disallowed = lambda c: not any([(s in c) for s in k['dont_allow']])
            choices = filter(lambda c: disallowed(c[0]), choices) 
            del k['dont_allow']
        k['choices'] = choices
        k['max_length'] = 8
        models.CharField.__init__(*a, **k)
    license = models.CharField(max_length=8)


class Game(BlogPost):
    LICENSES = get_licenses()
    
    # Download link
    crowdfund_link = models.URLField(blank=True,
                help_text='Link to crowd funding page (ie Kickstarter).')
    website_link = models.URLField(blank=True,
                help_text='Link to main page for video game.')
    source_link = models.URLField(blank=True,
                help_text='Link to sourcecode repo, ie github')

    download_link = models.URLField(blank=True,
                help_text='Direct link to download.')
    engine_is_free = models.BooleanField(default=False)
    content_is_free = models.BooleanField(default=False)
    #engine_license = models.CharField(choices=get_licenses(), max_length=32, default="prop")
    #content_license = models.CharField(choices=get_licenses(), max_length=32, default="prop")

    latest_version = models.CharField(max_length=64)
    update_date = models.DateTimeField(auto_now=True)
    cost = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=6,
                help_text="Cost in USD")

    class Meta:
        verbose_name = "Game post"
        verbose_name_plural = "Game posts"
        ordering = ("-update_date",)

